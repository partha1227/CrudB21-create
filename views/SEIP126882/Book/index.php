<?php

include_once('../../../vendor/autoload.php.');
use App\Bitm\SEIP126882\Book\Book;
use App\Bitm\SEIP126882\Book\Utility;


$book= new Book();
$allBook=$book->index();
//Utility::d($allBook);
?>

<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/css/bootstrap.css">
<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
<!--  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
</head>
<body>

<div class="container">
  <h2>Book List</h2>
    <div class="table-responsive">
  <table class="table">
    <thead>
      <tr>
        <th>SL#</th>
        <th>ID</th>
        <th>Book Title</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $sl=0;
    foreach($allBook as $book){
        $sl++;
        ?>
      <tr>
        <td><?php echo $sl; ?></td>
        <td><?php echo $book['id'] // for object: $book->id ; ?></td>
        <td><?php echo $book['title'] // for object: $book->title; ?></td>
        <td>
            <a href="view.php" class="btn btn-info" role="button">View</a>
            <a href="edit.php" class="btn btn-primary" role="button">Edit</a>
            <a href="delete.php" class="btn btn-danger" role="button">Delete</a>
        </td>


      </tr>
    <?php } ?>


    </tbody>
  </table>
  </div>
</div>

</body>
</html>
